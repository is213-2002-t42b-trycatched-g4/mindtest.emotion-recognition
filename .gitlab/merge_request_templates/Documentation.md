## What does this MR do?

(briefly describe what this MR is about)

## Moving docs to a new location?

- [ ] Make sure the old link is not removed and has its contents replaced with a link to the new location.
- [ ] Make sure internal links pointing to the document in question are not broken.
- [ ] Search and replace any links referring to old docs.
