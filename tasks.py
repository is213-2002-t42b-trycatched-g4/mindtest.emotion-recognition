#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from invoke import task, run

docs_dir = 'docs'
build_dir = os.path.join(docs_dir, '_build')


@task
def clean(ctx, bytecode=True, extra=''):
    patterns = []
    patterns.append('dist')
    patterns.append('build')
    patterns.append('builds')
    patterns.append('*.egg*')
    # patterns.append('data/*')

    if bytecode:
        patterns.append('**/*.orig*')
        patterns.append('**/*.pyc')
        patterns.append('*.pyc')
        patterns.append('*.json')
        patterns.append('**/__pycache__')
        patterns.append('__pycache__')
    if extra:
        patterns.append(extra)
    for pattern in patterns:
        run("rm -rf %s" % pattern)


@task(pre=[clean])
def build(ctx):
    run("python setup.py build && python setup.py egg_info")
    run("python setup.py sdist bdist_wheel")
    run("python setup.py install")


@task
def pep8(ctx):
    run("pycodestyle --first --count --config=setup.cfg **/*.py")


@task(pre=[pep8, clean])
def tests(ctx):
    run("python run_tests.py")


@task
def changelog(ctx):
    run("gitchangelog > CHANGELOG.md")
