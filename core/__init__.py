import os
from .jiazz import Jiazz

__version__ = 'v1.0rc1'
__license__ = 'nonfree'
__author__ = 'Edgar Castanheda'

PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
