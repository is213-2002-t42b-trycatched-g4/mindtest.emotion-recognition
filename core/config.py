import yaml
from os.path import dirname, abspath, join
from os import getenv


class objdict(dict):
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)


def load(routes=None):
    env = getenv('PYTHON_STAGE', 'dev')
    with open(join(dirname(abspath(__file__)),
                   '..', 'config', env + '.yml'), 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    tmp_cfg = cfg
    if routes:
        for route in routes.split('.'):
            tmp_cfg = tmp_cfg.get(route) if tmp_cfg.get(route) else tmp_cfg
    return objdict(tmp_cfg) if isinstance(tmp_cfg, dict) else tmp_cfg
