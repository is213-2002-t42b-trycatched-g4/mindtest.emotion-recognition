from sklearn.externals.joblib import load as loadPkl
from sklearn.externals.joblib import dump
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from retexto import ReTexto
import unicodedata as udat
import spacy
import unicodedata as udat
from spacy.symbols import ORTH, LEMMA, POS, TAG
nlp = spacy.load('es')

# DEFINE STOPWORDS
STOPWORDS = stopwords.words('spanish')
ES_STEMMER = SnowballStemmer('spanish')


def strip_accents_spain(
        string,
        accents=('COMBINING ACUTE ACCENT', 'COMBINING GRAVE ACCENT')):

    accents = set(map(udat.lookup, accents))
    chars = [c for c in udat.normalize('NFD', string) if c not in accents]
    return udat.normalize('NFC', ''.join(chars))


def moda(lst):
    return max(set(lst), key=lst.count)


class Jiazz(object):

    @staticmethod
    def pos_tagging(text):
        doc = nlp(text)

        tag_number = []
        tag_tense = []
        tag_gender = []
        tag_person = []
        tag_word = []
        tag_poss = []

        for token in doc:
            # print(token.text, token.tag_)
            if token.pos_ in [
                        'NOUN', 'ADJ', 'PRON', 'VERB', 'AUX', 'DET', 'PRON']:

                if token.pos_ in ['ADJ', 'VERB', 'PRON']:

                    if "Number=Sing" in token.tag_:
                        tag_number.append('sing')

                    if "Number=Plur" in token.tag_:
                        tag_number.append('plur')

                    if "Tense=Pres" in token.tag_:
                        tag_tense.append('pres')

                    if "Tense=Past" in token.tag_:
                        tag_tense.append('past')

                    if "Tense=Fut" in token.tag_:
                        tag_tense.append('fut')

                if token.pos_ in ['NOUN', 'ADJ']:
                    tag_word.append(token.text)

                if "Gender=Masc" in token.tag_:
                    tag_gender.append('m')

                if "Gender=Fem" in token.tag_:
                    tag_gender.append('f')

                if "Poss=Yes" in token.tag_:
                    tag_poss.append(True)

                if "Person=1" in token.tag_:
                    tag_person.append(1)

                if "Person=2" in token.tag_:
                    tag_person.append(2)

                if "Person=3" in token.tag_:
                    tag_person.append(3)

        return {
            'tag_number': moda(tag_number) if len(tag_number) else 'sing',
            'tag_gender': moda(tag_gender) if len(tag_gender) else 'm',
            'tag_tense': moda(tag_tense) if len(tag_tense) else 'pres',
            'tag_person': moda(tag_person) if len(tag_person) else 1,
            'tag_poss': moda(tag_poss) if len(tag_poss) else False,
            'pos_words': tag_word
        }

    @staticmethod
    def medium_stemmer(text):
        # CLEAN TEXT
        sample = ReTexto(strip_accents_spain(text))
        sentences = sample.lower() \
            .remove_html() \
            .remove_mentions() \
            .remove_tags() \
            .remove_smiles(by='smiling') \
            .remove_duplicate() \
            .remove_punctuation(by=' ') \
            .convert_emoji() \
            .convert_specials() \
            .remove_nochars() \
            .remove_stopwords() \
            .split_words()

        words = [ES_STEMMER.stem(w) for w in sentences]
        return ' '.join(words) if len(words) else ''

    @staticmethod
    def sanitize(text):
        text = ReTexto(strip_accents_spain(text))
        words = text.remove_html() \
            .remove_mentions() \
            .lower() \
            .remove_tags() \
            .remove_punctuation(by=' ') \
            .remove_smiles() \
            .convert_emoji() \
            .remove_nochars(preserve_tilde=True) \
            .remove_url() \
            .remove_multispaces() \
            .split_words()
        return ' '.join(words)

    @staticmethod
    def stemmer(text, stem=True):
        # CLEAN TEXT
        sample = ReTexto(strip_accents_spain(text))
        sentences = sample.lower() \
            .remove_html() \
            .remove_mentions() \
            .remove_tags() \
            .remove_smiles(by='smiling') \
            .remove_duplicate() \
            .remove_punctuation(by=' ') \
            .convert_emoji() \
            .convert_specials() \
            .remove_nochars() \
            .remove_stopwords() \
            .split_words()

        if stem is False:
            words = [w for w in sentences if w not in STOPWORDS]
            return ' '.join(words) if len(words) else ''

        words = [ES_STEMMER.stem(w) for w in sentences if w not in STOPWORDS]
        return ' '.join(words) if len(words) else ''

    @staticmethod
    def low_stemmer(text):
        # CLEAN TEXT
        sample = ReTexto(strip_accents_spain(text))
        sentences = sample.lower() \
            .remove_html() \
            .remove_mentions() \
            .remove_tags() \
            .remove_punctuation(by=' ') \
            .convert_emoji() \
            .split_words()
        return ''.join(sentences)

    @staticmethod
    def get_words(text):
        # CLEAN TEXT
        sample = ReTexto(text)
        sentences = sample.lower() \
            .remove_html() \
            .remove_mentions() \
            .remove_tags() \
            .remove_smiles(by='smiling') \
            .remove_punctuation(by=' ') \
            .convert_emoji() \
            .convert_specials() \
            .remove_nochars(preserve_tilde=True) \
            .remove_stopwords() \
            .split_words()

        words = [w for w in sentences if w not in STOPWORDS]
        return list(set(words))

    @classmethod
    def score(self, alg, text, stemmer=False):
        stemmer = stemmer if stemmer else self.stemmer
        stemmer_text = stemmer(text)
        predict_proba = getattr(alg, 'predict_proba', None)

        label = alg.predict([stemmer_text])[0]
        prediction = {"tag": label, "scores": {}}

        if predict_proba:
            scores = predict_proba([stemmer_text])[0]
            prediction["scores"] = dict(zip(alg.classes_, scores))
        return prediction

    @staticmethod
    def compress(obj, filename, compress_level=9):
        dump(obj, '/app/models/%s.pkl' % filename, compress=compress_level)
        return filename

    @staticmethod
    def load(filename):
        return loadPkl('/app/models/%s.pkl' % filename)
