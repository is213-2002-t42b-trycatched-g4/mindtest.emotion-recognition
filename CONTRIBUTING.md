How to contribute
-----------------

This guide is under heavy development. If you would like to contribute, please
see:

http://docs.python-guide.org/en/latest/notes/contribute/

How to test your changes
------------------------

``` bash
invoke tests
```

How to generate CHANGELOG
------------------------

``` bash
invoke changelog
```

Style Guide
-----------

For all contributions, please follow the `Guide Style Guide`:

http://docs.python-guide.org/en/latest/notes/styleguide/

``` bash
invoke pep8
```
