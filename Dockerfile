FROM edaniel15/docker-python-ml
ENV TERM xterm
LABEL Edux <edaniel15@gmail.com>

ARG stage=prod

RUN mkdir /app
WORKDIR /app
COPY . /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY ./nltk_dependencies.py /
RUN python nltk_dependencies.py

EXPOSE 80
CMD ["python", "server.py"]
