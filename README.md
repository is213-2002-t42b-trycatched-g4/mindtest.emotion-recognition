# API CLASSIFICATION TEXT
[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg)](https://github.com/conventional-changelog/standard-version)

## Developer Stage
### Pre-Install

 - Docker 1.*

### Run Dev

    cd /[project_path]
    docker build -t isc_ia_api .
    docker run -v $(pwd):/app:rw -it isc_ia_api <script>

### Run Production

    docke run -ti isc_ia_api --env PYTHON_STAGE=prod

### Metrics
At the moment this api provides four metrics (prometheus):

  * sanic_request_count - total number of requests (labels: method, endpoint, status) [counter]
  * sanic_request_latency_sec - request latency in seconds (labels: method, endpoint) [histogram]
  * sanic_mem_rss_bytes - resident memory used by the process (in bytes) [gague]
  * sanic_mem_rss_perc - a percent of total physical memory used by the process running Sanic [gague]
