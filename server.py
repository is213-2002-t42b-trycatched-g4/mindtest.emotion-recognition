from core import Jiazz as J
from sanic import Sanic
from sanic_cors import CORS, cross_origin
from sanic.response import json
from core import config


app = Sanic(__name__)
settings = config.load('settings')
cors = CORS(app, resources={r"/text*": {"origins": "*"}})

# LOAD MODELS
sentiment_esp = J.load('sentiment_esp_float')
gender_name_esp = J.load('gender_name_esp')
gender_gramatical_esp = J.load('gender_gramatical_esp')
emotions_esp = J.load('emotions_esp')
age_esp = J.load('age_esp')

intvoto_esp_float = J.load('intvoto_esp_float')
sentiment_esp_float = J.load('sentiment_esp_float')


@app.route("/")
async def default(request):
    data = {
        'name': 'IA Api for Text Classification',
        'models': [
            {'name': 'sentiment_esp', 'lang': 'es'},
            {'name': 'gender_name_esp', 'lang': 'es'},
            {'name': 'age_esp', 'lang': 'es'},
            {'name': 'emotions_esp', 'lang': 'es'}
        ],
        'status': 'ok',
        'version': settings.version,
        'debug': settings.debug,
        'author': settings.author
    }
    return data


@app.route('/text/all', methods=['POST', 'OPTIONS'])
async def all_models(request):
    post_data = request.json
    if post_data:
        text = post_data.get('text')
        post_name = post_data.get('name')

        gender = J.score(gender_name_esp, post_name, stemmer=J.low_stemmer)

        data = {
            'gender': gender.get('tag'),
            'sentiment': J.score(sentiment_esp, text).get('tag'),
            'emotions': J.score(emotions_esp, text),
            'age': J.score(age_esp, text).get('tag'),
            'words': J.get_words(text)
        }
        print(data)
        return json(data)
    return json({
        'error': True,
        'message': 'No Post Data Detected'},
        status=200
    )


@app.route('/text/politics', methods=['POST', 'OPTIONS'])
async def all_models(request):
    post_data = request.json
    if post_data:
        text = post_data.get('text')
        post_name = post_data.get('name') if 'name' in post_data else False
        _pos = J.pos_tagging(J.sanitize(text))

        clf_gender = 'm'
        if not post_name:
            clf_gender = J.score(
                gender_gramatical_esp,
                text).get('tag')
        else:
            clf_gender = J.score(
                gender_name_esp,
                post_name,
                stemmer=J.low_stemmer).get('tag')
        data = {
            'clf_gender': clf_gender,
            'clf_sentiment': J.score(sentiment_esp_float, text).get('tag'),
            'clf_intvoto': J.score(intvoto_esp_float, text).get('tag'),
            'clf_emotions': J.score(emotions_esp, text),
            'clf_age': J.score(age_esp, text).get('tag')
        }
        data.update(_pos)

        return json(data)
    return json({
        'error': True,
        'message': 'No Post Data Detected'},
        status=200
    )


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=settings.debug,
        # log_config=None,
        workers=settings.workers
    )
