#!/bin/bash
TAG=v1.0rc3
ECR_IMAGE_NAME=isc_ia_api
PROD_AWS_REGION=us-east-1
ECR_REPOSITORY=606769997744.dkr.ecr.us-east-1.amazonaws.com

export IMAGE_TAG=$ECR_IMAGE_NAME:$TAG
export IMAGE_NAME=$ECR_IMAGE_NAME
export PROD_ECR_REPOSITORY=$ECR_REPOSITORY
export ECR_IMAGE_TAG=$PROD_ECR_REPOSITORY/$IMAGE_NAME:$TAG

echo "LOGIN AWS | REGION: us-east-1"
echo "============================="
eval $(aws ecr get-login --region $PROD_AWS_REGION | sed -e 's/-e none//g')
echo "BUILD IMAGE"
echo "==========="
docker build -t $IMAGE_NAME . --build-arg stage=prod
echo "TAG IMAGE"
echo "==========="
docker tag $IMAGE_NAME:latest $ECR_IMAGE_TAG
echo "PUSH IMAGE"
echo "==========="
docker push $ECR_IMAGE_TAG
echo "DONE!"
echo "====="
date
