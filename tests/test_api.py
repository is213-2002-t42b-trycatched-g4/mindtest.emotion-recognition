# -*- coding: UTF-8 -*-
from nose.tools import assert_equals, assert_true
from unittest import TestCase
from server import app
import json


class ApiTest(TestCase):
    @classmethod
    def test_default_endpoint(self):
        request, response = app.test_client.get('/')
        assert_equals(response.status, 200)

    @classmethod
    def test_metrics_endpoint(self):
        request, response = app.test_client.get('/metrics')
        assert_equals(response.status, 200)

    @classmethod
    def test_text_endpoint(self):
        data = {
            'text': 'hola mundo',
            'name': 'edgar',
        }
        request, response = app.test_client.post(
            '/text/all',
            data=json.dumps(data)
        )
        assert_equals(response.status, 200)
