# -*- coding: UTF-8 -*-
from nose.tools import assert_equals, assert_true
from unittest import TestCase
from core import Jiazz


class UtilTest(TestCase):
    @classmethod
    def setUp(self):
        self.jiazz = Jiazz
        self.model = Jiazz.load('sentiment_esp')

    @classmethod
    def test_stemmer(self):
        text = 'hola que hace?'
        words = 'hol hac'
        assert_equals(self.jiazz.stemmer(text), words)

    @classmethod
    def test_low_stemmer(self):
        text = 'hola que hace?'
        words = 'holaquehace'
        assert_equals(self.jiazz.low_stemmer(text), words)

    @classmethod
    def test_scoring(self):
        text = 'hola que hace?'
        assert_true(
            self.jiazz.score(self.model, text),
            {}
        )
